FROM python:3-alpine

COPY pygments.patch .

RUN apk add gcc git musl-dev patch \
	# patch Pygments to use custom ABNF syntax
	&& git clone https://github.com/pygments/pygments.git \
	&& patch -u pygments/pygments/lexers/grammar_notation.py -i pygments.patch \
	# install patched Pygments
	&& cd pygments \
	&& python setup.py install \
	&& cd .. \
	# clean files
	&& rm -rf pygments pygments.patch \
	# install mkdocs and revision plugin
	&& pip install mkdocs-material mkdocs-git-revision-date-localized-plugin

# set working directory
WORKDIR /docs

# expose MkDocs development server port
EXPOSE 8000

# start development server by default
ENTRYPOINT ["mkdocs"]
CMD ["serve", "--dev-addr=0.0.0.0:8000"]
