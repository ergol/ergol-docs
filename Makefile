IMAGE_NAME = mkdocs-material
PORT = 8000

.PHONY: serve build build_mkdocs

serve:
	docker run --rm -it -p $(PORT):8000 -v $(PWD):/docs $(IMAGE_NAME)

build:
	docker run --rm -v $(PWD):/docs $(IMAGE_NAME) build

build_mkdocs:
	docker build -t $(IMAGE_NAME) .
