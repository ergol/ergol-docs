*[MUST NOT]: This phrase, or the phrase “SHALL NOT”, mean that the definition is an absolute prohibition of the specification.
*[SHALL NOT]: This phrase, or the phrase “MUST NOT”, mean that the definition is an absolute prohibition of the specification.

*[MUST]: This word, or the terms “REQUIRED” or “SHALL”, mean that the definition is an absolute requirement of the specification.
*[REQUIRED]: This word, or the terms “SHALL” or “MUST”, mean that the definition is an absolute requirement of the specification.
*[SHALL]: This word, or the terms “MUST” or “REQUIRED”, mean that the definition is an absolute requirement of the specification.

*[SHOULD NOT]: This phrase, or the phrase “NOT RECOMMENDED” mean that there may exist valid reasons in particular circumstances when the particular behavior is acceptable or even useful, but the full implications should be understood and the case carefully weighed before implementing any behavior described with this label.
*[NOT RECOMMENDED]: This phrase, or the phrase “SHOULD NOT” mean that there may exist valid reasons in particular circumstances when the particular behavior is acceptable or even useful, but the full implications should be understood and the case carefully weighed before implementing any behavior described with this label.

*[SHOULD]: This word, or the adjective “RECOMMENDED”, mean that there may exist valid reasons in particular circumstances to ignore a particular item, but the full implications must be understood and carefully weighed before choosing a different course.
*[RECOMMENDED]: This word, or the adjective “SHOULD”, mean that there may exist valid reasons in particular circumstances to ignore a particular item, but the full implications must be understood and carefully weighed before choosing a different course.

*[MAY]: This word, or the adjective “OPTIONAL”, mean that an item is truly optional. One vendor may choose to include the item because a particular marketplace requires it or because the vendor feels that it enhances the product while another vendor may omit the same item. An implementation which does not include a particular option MUST be prepared to interoperate with another implementation which does include the option, though perhaps with reduced functionality. In the same vein an implementation which does include a particular option MUST be prepared to interoperate with another implementation which does not include the option (except, of course, for the feature the option provides.)
*[OPTIONAL]: This word, or the adjective “MAY”, mean that an item is truly optional.  One vendor may choose to include the item because a particular marketplace requires it or because the vendor feels that it enhances the product while another vendor may omit the same item. An implementation which does not include a particular option MUST be prepared to interoperate with another implementation which does include the option, though perhaps with reduced functionality. In the same vein an implementation which does include a particular option MUST be prepared to interoperate with another implementation which does not include the option (except, of course, for the feature the option provides.)

*[ABNF]: Augmented Backus-Naur Form

*[LHS]: Left-Hand Side
*[RHS]: Right-Hand Side

*[AST]: Abstract Syntax Tree
