# Hello World!

## Creating a new project

First thing first, you must create a folder for your project. Let's name it `hello-world`.
Then, open a terminal, move into the folder, and type `ergol init`.
You are invited to inquire some information like the name of the program, it's ID, it's version, the author and the license.
Then, the following files are created:

```
hello-world
├── manifest.yml
└── src
    └── main.ergol
```

The `manifest.yml` file contains some information about the program.
It can also contains compiler options and the list of the program dependencies.


```yaml
program:
  id: hello-world
  name: Hello World
  version: 1.0.0
author:
  name: John Doe
  email: john.doe@example.com
legal:
  license: GPL-3-or-later
```

The `src/main.ergol` file is the entry point of the program.


## Coding

You can edit this file and use `print`, `printl` or `log` to make the program output a message.

=== "print"

	The `print` function prints a string as it (not followed by a line feed).

	```{.ergol}
	print("Hello World!")
	```

	output:

	```{.logs}
	Hello World!
	```

=== "printl"

	The `printl` function (for "print line") prints a string followed by a line feed (U+000A).

	```{.ergol}
	printl("Hello World!")
	```

	output:

	```{.logs}
	Hello World!⮐
	```

=== "log"

	The `log` function is useful to quickly print a formatted output.
	By default, the output is preceded by the [ISO 8601](https://en.wikipedia.org/wiki/ISO_8601) time and the thread name but [it can be customized](../../lib/logger/).

	```{.ergol}
	log("Hello World!")
	```

	output:

	```{.logs .iso8601-format}
	[%ISO_8601%][main] Hello World!⮐
	```

## Compiling and running

To compile the program simply type `ergol compile` in a terminal (at the project root).
Two new directories are created:

- `.cache`  
This directory contains cache data needed to speed up the next compilations.
- `dist`  
This directory contains the compiled program.

Finally, you can run the program.
In order to do so, assuming that your program name is `Hello World`, type the following in a terminal (at the project root):

=== "Linux / macOS"

	```bash
	./dist/Hello\ World
	```

=== "Windows"

	```dos
	dist/"Hello World.exe"
	```

<script src="../../javascripts/iso8601-format.js"></script>
