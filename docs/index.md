# Ergol Programming Language {: .main-title }

Ergol is a high-level general purpose programming language that is yet to be developed.

## :material-bullseye-arrow: A **very high-level** language that does not come with huge **performance** issues {: .goal }

The goal is to be able to write programs in a concise and elegant way, thanks to the extreme abstraction level (of the same order of magnitude as Python) that the language provides.
And so, while limiting performance losses.
In particular with the fact that the program is compiled and optimized, but not interpreted nor running in a virtual machine.

## :material-bullseye-arrow: A language that let you choose the **right balance** between execution **speed** and execution **safety** {: .goal }

Ergol implements by itself a lot of features to improve execution safety, like overflow checks and automatic memory management.
However, these kind features can be disabled to gain execution speed.

## :material-bullseye-arrow: **Secure** by default {: .goal }

By default, an Ergol program does not have any access to things like the file system, the network, or the execution of other programs.
These access can be granted using a config file, so you can precisely control what the executable can do and what it can access.

## :material-bullseye-arrow: A great **modularity** {: .goal }

Package managers are awesome.
Ergol comes with a NPM-like package manager that allows you to share code between your projects and use others code.
Don't reinvent the wheel!

## :material-bullseye-arrow: Simple **C bindings** {: .goal }

In order to easily access lower level stuff.

## :material-bullseye-arrow: A **WebAssembly** transpiler {: .goal }

It's possible to make an Ergol program run in a modern browser by transpiling Ergol to [WebAssembly](https://webassembly.org).
