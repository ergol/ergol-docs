```abnf
path: ; TODO

entity-import = identifier ["as" identifier]
star-import = "*" "as" identifier

import-statement = "from" path "import" (entity-import / star-import) *("," (entity-import / star-import))  ";"
```
