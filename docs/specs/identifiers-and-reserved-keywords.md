An identifier is a name chosen by the programer.
This name can be attributed to many things like variables, functions, classes, etc...

* An identifier MUST be a string of at least 1 character.
* An identifier MUST contains only [normal](../introduction#normal-characters) characters.
* An identifier MUST starts with any character in the Unicode categories `Ll`, `Lm`, `Lo`, `Lt`, `Lu` or `Nl`, or character U+0024 or U+005F.
* An identifier MUST NOT be a reserved keyword.

Reserved keywords:

<div class="table-list"></div>
* as
* bool
* char
* f32
* f64
* false
* fn
* from
* i16
* i32
* i64
* i8
* import
* in
* instanceof
* NaN
* NegInfinity
* not
* object
* PosInfinity
* rol
* ror
* true
* tuple
* type
* u16
* u32
* u64
* u8
* void

--8<-- "includes/abbreviations.md"
