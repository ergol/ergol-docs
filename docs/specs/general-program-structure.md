# General Program Structure

## Program

The program is a set of source files.
Each source file is dependent to a certain amount of other source files (could be 0).
When compiling, the programer choose to give one source file (the entry point) to the compiler.
All entry point's dependencies are resolved recursively and are then compiled with the entry point into one single executable.

!!! info
	In the specifications, the term “program” does not refer to the executable but to the set of source files.

## Source files

A source file start with any number of import statements and is followed by any number of body and export statements.
Export statements are not required to be at the end of the file, but they MUST be situated in the outer scope.

```abnf
body-statement = variable-declaration
body-statement /= side-effect-expression ";"
body-statement /= function-declaration / class-declaration / interface-declaration
body-statement /= block
body-statement /= conditional-blocks / switch-case / match
body-statement /= for-loop / while-loop / do-while-loop

source-file = *blank-character *import-statement *(body-statement / export-statement) *blank-character
```

--8<-- "includes/abbreviations.md"
