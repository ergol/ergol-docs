(function() {
	function pad(num, size) {
		var s = num + ''
		while (s.length < size) s = '0' + s
		return s
	}

	function iso8601() {
		var date = new Date();
		var y = pad(date.getUTCFullYear()    , 4)
		var m = pad(date.getUTCMonth() + 1   , 2)
		var d = pad(date.getUTCDate()        , 2)
		var h = pad(date.getUTCHours()       , 2)
		var i = pad(date.getUTCMinutes()     , 2)
		var s = pad(date.getUTCSeconds()     , 2)
		var l = pad(date.getUTCMilliseconds(), 3)

		return y
			+ '-'+ m
			+ '-' + d
			+ 'T' + h
			+ ':' + i
			+ ':' + s
			+ '.' + l 
			+ '000000Z'
	}

	document.querySelectorAll('.iso8601-format pre code').forEach(function(el) {
		el.textContent = el.textContent.replace(/%ISO_8601%/g, iso8601())
	})
})()
