# Roadmap

<figure>
	<img src="../images/roadmap.svg" alt="[infdev], indev, alpha, beta, release"></img>
</figure>

## Language

| Ergol development phase | task                                                                                                                       |
|-------------------------|----------------------------------------------------------------------------------------------------------------------------|
| infdev                  | writing the specifications                                                                                                 |
| indev                   | implementing the main features of the language                                                                             |
| indev to beta           | discussing about the language (RFC), modifying the specifications, implementing those changes                              |
| alpha                   | implementing the garbage collector                                                                                         |
| beta                    | finalizing the specifications and their implementation                                                                     |
| release                 | discussing about the language (RFC), modifying the specifications and implementing those changes in a retro-compatible way |

## Compiler

| Ergol development phase | task                                         |
|-------------------------|----------------------------------------------|
| indev                   | implementing a first compiler in Java        |
| indev                   | re-implementing the compiler in Ergol itself |
| beta                    | adding the possibility to output WebAssembly |


## Package manager

| Ergol development phase | task                                          |
|-------------------------|-----------------------------------------------|
| alpha                   | developing the package manager                |
| beta                    | developing a website to list all the packages |
